from selenium import webdriver
import requests
import string
import random
import json
from Spotify import Spotify



def id_generator(size=6, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def run():
    config = json.load(open('config.json'))
    driver = webdriver.Chrome("chromedriver.exe")
    email = config['email_prefix'] +  "+" + id_generator(size=8) + "@gmail.com"
    print (email)
    password = config['password']
    spotify = Spotify(driver,config['name'],email,password)
    spotify.signUp()

    
    print ("EMAIL: " +  email + "\n" + "Password: " + password)


if __name__ == "__main__":
    run()
