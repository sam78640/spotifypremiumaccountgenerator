class Spotify:
    def __init__(self,driver,name,email,password):
        self.driver = driver
        self.name = name
        self.email = email
        self.password = password
    
    def signUp(self):
        import random
        import time
        url = "https://www.spotify.com/uk/signup/"
        page = self.driver.get(url)

        # -!- Find input boxes -!-
        email = self.driver.find_element_by_id('register-email')
        confirm_email = self.driver.find_element_by_id('register-confirm-email')
        password = self.driver.find_element_by_id('register-password')
        name = self.driver.find_element_by_id('register-displayname')
        dob_day = self.driver.find_element_by_id('register-dob-day')
        dob_mon = self.driver.find_element_by_id('register-dob-month')
        dob_year = self.driver.find_element_by_id('register-dob-year')
        gender = self.driver.find_element_by_id('register-male')

        # -!- Send the details -!-
        email.send_keys(self.email)
        confirm_email.send_keys(self.email)
        password.send_keys(self.password)
        name.send_keys(self.name)
        dob_day.send_keys(random.randint(10,29))
        dob_mon.send_keys("August")
        dob_year.send_keys("1998")
        gender.click()

        # -!- Wait for captcha -!-
        captcha = self.driver.find_elements_by_tag_name('iframe')[4]
        self.driver.switch_to_frame(captcha)
        captchaFilled = False
        while not captchaFilled:
            captchaFilled = self.checkCaptcha(self.driver.page_source)
        self.driver.switch_to_default_content()
        if captchaFilled:
            signupButton = self.driver.find_element_by_id('register-button-email-submit')
            signupButton.click()
            time.sleep(3)
        self.startTrial()
        self.cancelSubscription()
    

    def startTrial(self):
        import time
        import json
        config = json.load(open('config.json'))
        driver = self.driver
        url = "https://www.spotify.com/uk/purchase/panel/#__main-pci-credit-card"
        driver.get(url)
        time.sleep(7)

        #Login
        email = driver.find_element_by_id('login-username')
        password = driver.find_element_by_id('login-password')

        email.send_keys(self.email)
        password.send_keys(self.password)

        submit = driver.find_element_by_tag_name('button')
        submit.click()

        time.sleep(5)
        #End login

        card_number = driver.find_element_by_id('cardnumber')
        expiry_mon = driver.find_element_by_id('expiry-month')
        expiry_year = driver.find_element_by_id('expiry-year')
        security_code = driver.find_element_by_id('security-code')

        # -*- Send details -*-
        card_number.send_keys(config['card_no'])
        expiry_mon.send_keys(config['expiry_month'])
        expiry_year.send_keys(config['expiry_year'])
        security_code.send_keys(config['cvv'])

        button = driver.find_elements_by_css_selector('purchasebutton')
        time.sleep(5)
    
    def cancelSubscription(self):
        url = "https://www.spotify.com/uk/account/subscription/change/intent/?target=free"
        driver = self.driver
        driver.get(url)
        time.sleep(3)
        yes_button = driver.get_element_by_css_selector('btn-black') 
        yes_button.click()
        time.sleep(4)

    def checkCaptcha(self,source):
        from bs4 import BeautifulSoup  
        soup = BeautifulSoup(source,'html.parser')
        span = soup.findAll('span',{'class': 'recaptcha-checkbox'})[0]
        checked = span['aria-checked']
        if checked == "true":
            return True
        else:
            return False